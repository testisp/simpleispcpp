#ifndef _PIPELINE_
#define _PIPELINE_
#include "util.h"
class ISPPipeline {
private:
    uint16_t* rawdata = nullptr;
    uint32_t width = 0;
    uint32_t height = 0;
    ISPParameter* isppara = nullptr;
public:
    ISPPipeline() {}
    ISPPipeline(uint16_t* rawdata, uint32_t width, uint32_t height, ISPParameter* isppara);
    virtual ISPResult ExcuteProcess()=0;
    uint16_t* GetRawdata();
    uint32_t GetWidth();
    uint32_t GetHeight();
    ISPParameter* ISPPipeline::GetISPPara();
};
#endif
