#ifndef _PARAMETER_
#define _PARAMETER_

#ifndef TRUE
#define TRUE 1
#endif

#ifndef true
#define true 1
#endif

#ifndef false
#define false 0
#endif

#ifndef FALSE
#define FALSE 0
#endif


#ifndef uint8_t
#define uint8_t unsigned char
#endif

#ifndef int8_t
#define int8_t char
#endif

#ifndef uint16_t
#define uint16_t unsigned short
#endif

#ifndef int16_t
#define int16_t short
#endif

#ifndef uint32_t
#define uint32_t unsigned int
#endif

#ifndef int32_t
#define int32_t int
#endif

#ifndef ISPResult
#define ISPResult int
#endif


#ifndef UINT32
#define UINT32 unsigned int
#endif

#ifndef INT32
#define INT32 int
#endif

#ifndef int64_t
#define int64_t long long
#endif

#ifndef uint64_t
#define uint64_t unsigned long long
#endif


//typedef struct tagChannelType {
//	const char* r = "r";
//	const char* gr = "gr";
//	const char* gb = "gb";
//	const char* b = "b";
//} ChannelType;


typedef struct tagBLC_PARAM {
	uint32_t blcValue;
} BLC_PARAM;

typedef struct tagLSC_PARAM {
	uint32_t numH;
	uint32_t numW;
	uint32_t rGain[13][17];
	uint32_t grGain[13][17];
	uint32_t gbGain[13][17];
	uint32_t bGain[13][17];
} LSC_PARAM;

typedef struct tagInterpChannel {
	uint32_t** rInterp = nullptr;
	uint32_t** grInterp = nullptr;
	uint32_t** gbInterp = nullptr;
	uint32_t** bInterp = nullptr;
}Interpchannel;

typedef struct tagWB_PARAM {
	float rGain;
	float gGain;
	float bGain;
} WB_PARAM;

typedef struct tagCCM_PARAM {
	float CCM[3][3];
} CCM_PARAM;

typedef struct tagGAMMA_PARAM {
	uint16_t lut[1024];
} GAMMA_PARAM;

class ISPParameter{
public:
	GAMMA_PARAM gammapara;
	CCM_PARAM ccmpara;
	WB_PARAM wbpara;
	LSC_PARAM lscpara;
	BLC_PARAM blcpara;
	ISPParameter();
	~ISPParameter();

};



#endif