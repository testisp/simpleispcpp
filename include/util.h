#ifndef max
#define max(a,b)            (((a) > (b)) ? (a) : (b))
#endif

#ifndef min
#define min(a,b)            (((a) < (b)) ? (a) : (b))
#endif

#ifndef clip
#define clip(x,a,b)  (max((a),min((x),(b))))
#endif


#ifndef _UTIL_
#define _UTIL_


#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/highgui/highgui_c.h>
#include <opencv2/imgproc/imgproc.hpp>
#include "include/Parameter.h"

typedef struct tagInputDataInfo {
	uint32_t Width; // image data width
	uint32_t Height; // image data height
	char* InputPath; // image data path
	char* OutputPath;
} InputDataInfo;

ISPResult ReadMipiRaw(uint8_t* mipiraw, uint32_t mipirawsize, InputDataInfo& intputinfo);
ISPResult ConvertUint10(uint8_t* mipidata, uint16_t* rawdata, uint32_t& width, uint32_t& height);
ISPResult ParserCommandLine(int argc, char** argv, InputDataInfo& inputdatainfo, ISPParameter* isppara);
void PrintUsage(char* AppName);
bool ValidateCommand(int argc, char** argv);
bool CheckCmdOption(char* pArg);
void Convert10To8(uint16_t* src, uint8_t* dst, uint32_t width, uint32_t height);
cv::Mat ToMat(uint8_t* databuffer, uint32_t& width, uint32_t& height, uint32_t channel);
uint32_t SimpleDivid(const uint32_t& dividend, const uint32_t& divisor);
uint32_t Divid(const uint32_t& dividend, const uint32_t& divisor);
void PadAsReflect(uint16_t* input, uint16_t* output, uint32_t w, uint32_t h, uint8_t expand);


// em... The original intention was to get the length of the two-dimensional array
// but encountered the problem of array degeneracy and abandoned
template <typename T>
uint32_t GetArrayRow(T nums) {
    uint32_t a = sizeof(nums);
    uint32_t b = sizeof(nums[0][0]);

    uint32_t total = SimpleDivid(sizeof(nums), sizeof(nums[0][0]));
    uint32_t row = SimpleDivid(sizeof(nums), sizeof(nums[0]));
    return row;
}
template <typename T>
uint32_t GetArrayCol(T nums) {
    uint32_t total = SimpleDivid(sizeof(nums), sizeof(nums[0][0]));
    uint32_t row = SimpleDivid(sizeof(nums), sizeof(nums[0]));
    uint32_t col = total / row;
    return col;
}

#endif
