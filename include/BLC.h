﻿#ifndef _BLC_
#define _BLC_

#include "util.h"
#include "include/Pipeline.h"

class BLC :public ISPPipeline {
private:
    BLC_PARAM blcpara;

public:
    BLC(uint16_t* rawdata, uint32_t width, uint32_t height, ISPParameter* isppara) :ISPPipeline(rawdata, width, height, isppara) {
        this->blcpara = GetISPPara()->blcpara;
    }
    virtual ISPResult ExcuteProcess();
};


#endif