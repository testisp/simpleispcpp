#ifndef _LSC_
#define _LSC_
#include "util.h"
#include "include/Pipeline.h"

class LSC :public ISPPipeline {
private:
    LSC_PARAM* lscpara = nullptr;
    //interpolated result
    Interpchannel interpchannel;
    typedef struct tagBlockCorner {
        uint32_t bx0;
        uint32_t bx1;
        uint32_t by0;
        uint32_t by1;
    }BlcokCorner;
    // interplate parameter
    typedef struct tagInterpPara{
        uint32_t blockX; // locate which block
        uint32_t blockY;
        uint32_t curY; // relative coordinate in block
        uint32_t curX;
        uint32_t w1; // bilinear weight
        uint32_t w2; 
        uint32_t w3; 
        uint32_t w4;
    }InterpPara;

 
public:
    LSC(){}
    LSC(uint16_t* rawdata, uint32_t width, uint32_t height, ISPParameter* isppara) :ISPPipeline(rawdata, width, height, isppara) {
        this->lscpara = &(this->GetISPPara()->lscpara);
    };
    virtual ISPResult ExcuteProcess();
    uint32_t GetinterpValue(const InterpPara& interppara, char* channel, const BlcokCorner& corner);

};


#endif