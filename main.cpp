#include "include/util.h"
#include "include/Pipeline.h"
#include "include/BLC.h"
#include "include/LSC.h"

#define INPUTPATH "E:/test_pipeline/simpleISPCPP/inimg/chart_toyworld_d65_1500lux_016.RAWMIPI"
#define OUTPUTPATH "E:/test_pipeline/simpleISPCPP/outimg/"
#define WIDTH 4000
#define HEIGHT 3000

using namespace cv;
using namespace std;




int main(int argc, char** argv) {

    ISPParameter* ispparameter = new ISPParameter(); // ISP default parameter
    InputDataInfo inputdatainfo = {WIDTH, HEIGHT, INPUTPATH,OUTPUTPATH}; // input data default setting
    // commandline parser to get pipneline parameter or use default.
    ParserCommandLine(argc, argv, inputdatainfo, ispparameter); // get commandline custom parameter and setting
    
    uint32_t width = inputdatainfo.Width;
    uint32_t height = inputdatainfo.Height;
    uint32_t resulotion = width * height;
    uint8_t* mipirawdata = new uint8_t[resulotion * 5/4];
    uint16_t* rawdata = new uint16_t[resulotion];
    //ISPPipeline isppipe = new ISPPipeline(); // get a isp pipeline instance
    string outpath(OUTPUTPATH);
    bool succeed;
    Mat resimg; // show the result image
    ISPPipeline* isppipe;

    succeed = ReadMipiRaw(mipirawdata, resulotion * 5 / 4, inputdatainfo);
    if (succeed) {
        succeed = ConvertUint10(mipirawdata, rawdata, width, height);
        
        uint8_t* lowerbit = new uint8_t[resulotion];
       /* Convert10To8(rawdata, lowerbit, width, height);
        resimg = ToMat(lowerbit, width, height, 1);
        imwrite(outpath + "rawdata.png", resimg);*/

        isppipe = new BLC(rawdata, width, height, ispparameter);
        isppipe->ExcuteProcess();

        /*Convert10To8(rawdata, lowerbit, width, height);
        resimg = ToMat(lowerbit, width, height, 1);
        imwrite(outpath + "blcdata.png", resimg);*/

        isppipe = new LSC(rawdata, width, height, ispparameter);
        isppipe->ExcuteProcess();
        
        Convert10To8(rawdata, lowerbit, width, height);
        resimg = ToMat(lowerbit, width, height, 1);
        imwrite(outpath + "lsc-data.png", resimg);

    }
    
   


    return 0;
 
}
