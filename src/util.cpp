#include "include/util.h"
using namespace std;
using namespace cv;



ISPResult ReadMipiRaw(uint8_t* mipiraw, uint32_t mipirawsize, InputDataInfo& intputinfo) {
    /**
    * Description: read mipi raw data as 8 bits format
    * param:
    * return:
    */
    ISPResult succeed = false;

    fstream mipifile(intputinfo.InputPath, ios::in | ios::binary);
    if (mipifile.fail()) {
        cout << __FUNCTION__ << " Open MIPIRAW failed!" << endl;
        return succeed;
    }
    mipifile.seekg(0, ios::end);
    streampos filesize = mipifile.tellg();
    uint32_t cursize = static_cast<uint32_t>(filesize);
    cout << __FUNCTION__ << " Raw size:" << filesize << endl;
    mipifile.seekg(0, ios::beg);

    if (mipirawsize >= cursize) {
            mipifile.read((char*)mipiraw, mipirawsize);
            succeed = true;
    }
    else {
        cout << __FUNCTION__ << " Invalid buffer size!" << endl;
        mipiraw = nullptr;
    }

    mipifile.close();
    return succeed;

}

ISPResult ConvertUint10(uint8_t* mipidata, uint16_t* rawdata, uint32_t& width, uint32_t& height) {
	/**
	* Description: convert to 10 bits by re - group every 5 bytes(5 x 8 = 4 x 10).
	* param:
	* return: 
	*/
    ISPResult succeed = false;
	for (int i = 0; i < width * height * 5 / 4; i+=5) {
		rawdata[i * 4 / 5] = ((uint32_t)mipidata[i] << 2) + ((uint32_t)mipidata[i + 4] & 0x3);
		rawdata[i * 4 / 5 + 1] = ((uint32_t)mipidata[i+1] << 2) + ((uint32_t)mipidata[i + 4] >> 2 & 0x3);
		rawdata[i * 4 / 5 + 2] = ((uint32_t)mipidata[i+2] << 2) + ((uint32_t)mipidata[i + 4] >> 4 & 0x3);
		rawdata[i * 4 / 5 + 3] = ((uint32_t)mipidata[i+3] << 2) + ((uint32_t)mipidata[i + 4] >> 6 & 0x3);
	}
    succeed = true;
	cout << "**** mipi data decode done. ****" << endl;
    return succeed;

}


int ParserCommandLine(int argc, char** argv, InputDataInfo& inputdatainfo, ISPParameter* isppara){
    /**
    * Description: parse the command-line arguments          
    * param:
    * return:
    */
   

    if (!argv)
    {
        return false;
    }

    cout << endl;

    if (1 == argc)// if no input argv, output the help info
    {
        PrintUsage(argv[0]);
        return false;
    }

    if (!ValidateCommand(argc, argv)) // check the argv match or not
    {
        return false;
    }
    for (int i = 1; i < argc; i++)
    {
        if (!strcmp(_strlwr(argv[i]), "-i"))//load input path
        {
            if (i + 1 < argc)
            {
                strcpy(inputdatainfo.InputPath, argv[i + 1]);
            }
            else
            {
                printf("error, please specify input file path\n");
                return false;
            }
        }
        if (!strcmp(_strlwr(argv[i]), "-o"))//load ae path
        {
            if (i + 1 < argc)
            {
                strcpy(inputdatainfo.OutputPath, argv[i + 1]);
            }
            else
            {
                printf("error, please specify output file path\n");
                return false;
            }
        }
        if (!strcmp(_strlwr(argv[i]), "-w"))//save ae path
        {
            if (i + 1 < argc)
            {
                char warg[20];
                strcpy(warg, argv[i + 1]);
                inputdatainfo.Width = atoi(warg);
                //cout << isppara->blcpara.blcValue << endl;
            }
            else
            {
                printf("error, please specify black level correct argument\n");
                return false;
            }
        }
        if (!strcmp(_strlwr(argv[i]), "-h"))//save ae path
        {
            if (i + 1 < argc)
            {
                char harg[20];
                strcpy(harg, argv[i + 1]);
                inputdatainfo.Height = atoi(harg);
                //cout << isppara->blcpara.blcValue << endl;
            }
            else
            {
                printf("error, please specify black level correct argument\n");
                return false;
            }
        }
        if (!strcmp(_strlwr(argv[i]), "-blc"))//save ae path
        {
            if (i + 1 < argc)
            {
                char blcarg[20];
                strcpy(blcarg, argv[i + 1]);
                isppara->blcpara.blcValue = atoi(blcarg);
                //cout << isppara->blcpara.blcValue << endl;
            }
            else
            {
                printf("error, please specify black level correct argument\n");
                return false;
            }
        }
        if (!strcmp(_strlwr(argv[i]), "-wbc"))//save ae path
        {
            if (i + 1 < argc)
            {
                string wbcarg(argv[i + 1]);
                stringstream ss(wbcarg);
                string eacharg;
                float num[3] = {0, 0, 0};
                int i = 0;
                while (getline(ss, eacharg, ',')) { // convert string to float, split as ','
                    istringstream stof(eacharg);
                    stof >> num[i];
                    i++;
                }
                isppara->wbpara = {num[0], num[1], num[2]};
                //cout << isppara->wbpara.rGain << " " << isppara->wbpara.gGain << " " << isppara->wbpara.bGain << endl;
            }
            else
            {
                printf("error, please specify white balance gain argument\n");
                return false;
            }
        }
    }

    
    return true;
}


void PrintUsage(char* AppName)
{
    cout<< AppName << " [-i input_image] [-o output_img]" <<endl;
    cout << "[-w imgage weight x] [-h image height x]" << endl;
    cout<< "[-blc black level correction as x] [-wbc white balance gain as x,x,x]" <<endl;

}

bool ValidateCommand(int argc, char** argv)
{
    int i, j;

    bool bRet = true;

    for (i = 1; i < argc; i++)
    {
        if (argv[i][0] == '-')
        {
            bRet = CheckCmdOption(argv[i]);
            if (!bRet)
            {
                cout<< "error, unknown command option "<< argv[i] <<endl;
                cout << endl;
                PrintUsage(argv[0]);
                return false;
            }
        }
    }
    return true;
}

bool CheckCmdOption(char* pArg)
{
    const static string AcceptedCommand[6]= {"-i", "-o", "-w", "-h", "-blc", "-wbc"};
    bool ret = false;

    for (int j = 0; j < 6; j++)
    {
        if (!strcmp(_strlwr(pArg), AcceptedCommand[j].c_str()))
        {
            ret = true;
            break;
        }
    }

    return ret;
}

Mat ToMat(uint8_t* databuffer, uint32_t& width, uint32_t& height, uint32_t channel) {
    cv::Mat ret;
    if (channel == 3){
            ret = Mat::zeros(Size(width, height), CV_8UC3);
    }
    else if (channel == 1){
            ret = Mat::zeros(Size(width, height), CV_8UC1);     
    }

    for (int i = 0; i < height; ++i){
        uint8_t* data = ret.ptr<unsigned char>(i);
        //unsigned char* pSubBuffer = databuffer + (height - 1 - i) * width * channel;
        unsigned char* pSubBuffer = databuffer + i * width * channel;
        memcpy(data, pSubBuffer, width * channel);
    }
    if (channel == 1){
        cv::cvtColor(ret, ret, COLOR_GRAY2BGR);
    }
    else if (channel == 3){
        cv::cvtColor(ret, ret, COLOR_RGB2BGR);
    }

    return ret;
}

void Convert10To8(uint16_t* src, uint8_t* dst, uint32_t width, uint32_t height) {
    for (int i = 0; i < height; i++) {
        for (int j = 0; j < width; j++) {
            // use each pixel divid 4 and clip to [0, 255]
            if ((src[i * width + j] >> 2) > 255)
                dst[i * width + j] = 255;
            else if ((src[i * width + j] >> 2) < 0)
                dst[i * width + j] = 0;
            else
                dst[i * width + j] = (src[i * width + j] >> 2) & 255;
        }
    }
}


uint32_t SimpleDivid(const uint32_t& dividend, const uint32_t& divisor) {
    uint32_t ret = 0;
    if (dividend < divisor) return ret;
    uint32_t x = dividend;
    uint32_t y = divisor;
    while (x >= y)
    {
        x -= y;
        ++ret;
    }
    return ret;
}

uint32_t Divid(const uint32_t& dividend, const uint32_t& divisor){
    /// see as: https://leetcode.com/problems/divide-two-integers/
    /// 
    
    if (dividend == INT_MIN && divisor == -1) {
        return INT_MAX;
    }
    long dvd = labs(dividend), dvs = labs(divisor), ans = 0;
    int sign = dividend > 0 ^ divisor > 0 ? -1 : 1;
    while (dvd >= dvs) {
        long temp = dvs, m = 1;
        while (temp << 1 <= dvd) {
            temp <<= 1;
            m <<= 1;
        }
        dvd -= temp;
        ans += m;
    }
    return sign * ans;
    }


void PadAsReflect(uint16_t* input, uint16_t* output, uint32_t w, uint32_t h, uint8_t expand){
    /// see as:https://codereview.stackexchange.com/questions/120203/efficient-image-padding-in-c
    /// 
    /// 
    ///
    uint32_t padW, padH;
    uint16_t* inputbuf = input;
    padH = h + 2 * expand;
    padW = w + 2 * expand;
    uint32_t buf_size = sizeof(uint16_t) * padW * padH;
    output = (uint16_t*)malloc(buf_size);
    
    //TODO: Determine that the Pad is less than the edge length of the image
    for (int i = expand; i != (padH - expand); i++) {

        for (int j = 0; j != expand; j++) // Assigns value to the left pad position, from the inside to out
            output[i * padW + (expand - 1 - j)] = inputbuf[(i - expand) * w + j + 1]; 

        for (int j = expand; j != padW - expand; j++) // Assigns value to the original corresponding position 
            output[i * padW + j] = input[(i - expand) * w + (j - expand)];

        for (int j = 0; j != expand; j++) // Assigns value to the right pad position, from the inside to out
            output[i * padW + padW - expand + j] = output[i * padW + padW - expand - 2 - j];
    }

    for (int i = 0; i != expand; i++) { // Assigns value to the up and down pad position, from the inside to out
        memcpy(&output[(expand - 1) * padW - i * padW], &output[expand * padW  + (i+1) * padW], sizeof(uint16_t) * padW);
        memcpy(&output[(padH - expand) * padW + i * padW], &output[(padH - expand - 1) * padW - (i+1) * padW], sizeof(uint16_t) * padW);

    }
}