﻿
#include "include/BLC.h"

using namespace std;


ISPResult BLC::ExcuteProcess() {
	uint16_t* rawdata = GetRawdata();
	int32_t imwidth = GetWidth();
	int32_t imheight = GetHeight();
	for (int32_t i = 0; i < imwidth * imheight; i++) {
		rawdata[i] -= this->blcpara.blcValue;
	}
	return true;
}