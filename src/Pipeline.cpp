#include "include/Pipeline.h"

ISPPipeline::ISPPipeline(uint16_t* rawdata, uint32_t width, uint32_t height, ISPParameter* isppara) {
    this->rawdata = rawdata;
    this->width = width;
    this->height = height;
    this->isppara = isppara;
}


uint16_t* ISPPipeline::GetRawdata() {
    return this->rawdata;
}

uint32_t ISPPipeline::GetWidth() {
    return this->width;
}
uint32_t ISPPipeline::GetHeight() {
    return this->height;
}

ISPParameter* ISPPipeline::GetISPPara() {
    return this->isppara;
}
ISPResult ISPPipeline::ExcuteProcess() {
    return true;
}