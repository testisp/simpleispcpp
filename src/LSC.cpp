#include "include/LSC.h"



ISPResult LSC::ExcuteProcess() {
    uint16_t* rawdata = GetRawdata();
    uint32_t imwidth = GetWidth();
    uint32_t imheight = GetHeight();
    uint32_t halfW = imwidth >> 1;
    uint32_t halfH = imheight >> 1;
    //numbers of block
    uint32_t intervalH = SimpleDivid(halfH, this->lscpara->numH - 1);
    uint32_t intervalW = SimpleDivid(halfW, this->lscpara->numW - 1);
    uint32_t partition = intervalH * intervalW; // bilinear interplate partition coefficient
    interpchannel.rInterp = new uint32_t*[halfH];
    interpchannel.grInterp = new uint32_t*[halfH];
    interpchannel.gbInterp = new uint32_t*[halfH];
    interpchannel.bInterp = new uint32_t*[halfH];
    // block's 4 corner point coordinates
    BlcokCorner corner;
    corner.bx0 = 0;
    corner.bx1 = intervalW - 1;
    corner.by0 = 0;
    corner.by1 = intervalH - 1;
    InterpPara interppara;
    // using fix-point as Q10 Calibrations  
    for (uint32_t i = 0; i < halfH; i++) {
        interpchannel.rInterp[i] = new uint32_t[halfW];
        interpchannel.grInterp[i] = new uint32_t[halfW];
        interpchannel.gbInterp[i] = new uint32_t[halfW];
        interpchannel.bInterp[i] = new uint32_t[halfW];
        for (uint32_t j = 0; j < halfW; j++) {
            // locate which block
            interppara.blockY = SimpleDivid(i, intervalH);
            interppara.blockX = SimpleDivid(j, intervalW);
            // relative coordinate in block
            interppara.curY = i - interppara.blockY * intervalH;
            interppara.curX = j - interppara.blockX * intervalW;
            // calculate the bilinear weight
            interppara.w1 = (corner.bx1 - interppara.curX) * (corner.by1 - interppara.curY);
            interppara.w2 = (corner.bx1 - interppara.curX) * (interppara.curY - corner.by0);
            interppara.w3 = (interppara.curX - corner.bx0) * (corner.by1 - interppara.curY);
            interppara.w4 = (interppara.curX - corner.bx0) * (interppara.curY - corner.by0);
            
            uint32_t avg;
            uint32_t norm;
            // interplate r channel lsc coefficient
            avg = GetinterpValue(interppara, "r", corner);
            norm = Divid(avg, partition);
            interpchannel.rInterp[i][j] = norm;
            // interplate gr channel lsc coefficient
            avg = GetinterpValue(interppara, "gr", corner);
            norm = Divid(avg, partition);
            interpchannel.grInterp[i][j] = norm;
            // interplate gb channel lsc coefficient
            avg = GetinterpValue(interppara, "gb", corner);
            norm = Divid(avg, partition);
            interpchannel.gbInterp[i][j] = norm;
            // interplate b channel lsc coefficient
            avg = GetinterpValue(interppara, "b", corner);
            norm = Divid(avg, partition);
            interpchannel.bInterp[i][j] = norm;

            // correct rawdata using calculated lsc coefficient above
            rawdata[2 * imwidth * i + 2 * j] = 
                (uint32_t)((float)rawdata[2 * imwidth * i + 2 * j] * (float)(interpchannel.rInterp[i][j] >> 10)); // r
            rawdata[2 * imwidth * i + 2 * j + 1] = 
                (uint32_t)((float)rawdata[2 * imwidth * i + 2 * j + 1] * (float)(interpchannel.grInterp[i][j] >> 10)); // gr
            rawdata[(2 * i + 1) * imwidth + 2 * j] = 
                (uint32_t)((float)rawdata[(2 * i + 1) * imwidth + 2 * j] * (float)(interpchannel.gbInterp[i][j] >> 10)); // gb
            rawdata[(2 * i + 1) * imwidth + 2 * j + 1] = 
                (uint32_t)((float)rawdata[(2 * i + 1) * imwidth + 2 * j + 1] * (float)(interpchannel.bInterp[i][j] >> 10)); // b         
        }
    }
    return true;
}
uint32_t LSC::GetinterpValue(const InterpPara& interppara, char* channel, const BlcokCorner& corner) {
    
    // get the 4 corner coordinates at red channel
    uint32_t p1, p2, p3, p4;
    if (strcmp(_strlwr(channel),"r")) {
        p1 = lscpara->rGain[interppara.blockY][interppara.blockX];
        p2 = lscpara->rGain[interppara.blockY+1][interppara.blockX];
        p3 = lscpara->rGain[interppara.blockY][interppara.blockX+1];
        p4 = lscpara->rGain[interppara.blockY+1][interppara.blockX+1];
    }
    else if (strcmp(_strlwr(channel), "gr")) {
        p1 = lscpara->grGain[interppara.blockY][interppara.blockX];
        p2 = lscpara->grGain[interppara.blockY + 1][interppara.blockX];
        p3 = lscpara->grGain[interppara.blockY][interppara.blockX + 1];
        p4 = lscpara->grGain[interppara.blockY + 1][interppara.blockX + 1];
    }
    else if (strcmp(_strlwr(channel), "gb")) {
        p1 = lscpara->gbGain[interppara.blockY][interppara.blockX];
        p2 = lscpara->gbGain[interppara.blockY + 1][interppara.blockX];
        p3 = lscpara->gbGain[interppara.blockY][interppara.blockX + 1];
        p4 = lscpara->gbGain[interppara.blockY + 1][interppara.blockX + 1];
    }
    else if(strcmp(_strlwr(channel), "b")) {
        p1 = lscpara->bGain[interppara.blockY][interppara.blockX];
        p2 = lscpara->bGain[interppara.blockY + 1][interppara.blockX];
        p3 = lscpara->bGain[interppara.blockY][interppara.blockX + 1];
        p4 = lscpara->bGain[interppara.blockY + 1][interppara.blockX + 1];
    }
    uint32_t avg = interppara.w1 * p1 + interppara.w2 * p2 + interppara.w3 * p3 + interppara.w4 * p4;
    return avg;
}